﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class A
{
    public int m_ID;

    public void Run() { }
}

public class NewBehaviourScript : MonoBehaviour
{
    void Start()
    {
        Run();
        Debug.LogError("111");
        A a = new A();
        var dest = a ?? null;
        a?.Run();
    }

    public async void Run()
    {
        await Task.Delay(1000);
        int a = await Call(0);
        Debug.LogError("Run() " + a);
    }

	public Task<int> Call(int request)
    {
        var tcs = new TaskCompletionSource<int>();
        tcs.SetResult(request);
        return tcs.Task;
	}

	// Update is called once per frame
	void Update()
    {

    }
}
